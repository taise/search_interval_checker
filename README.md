# Search Interval Checker

これは特定のサイトに対して繰り返しアクセスを行ない、  
BANされるまでの間隔をチェックするためのツールです。  

悪用厳禁

対象のサイトに対して繰り返しアクセスを行ない、どのタイミングでBANされるか、  
また、どのタイミングでBANが解消されるかをチェックするためのものです。

## System dependency

* python 3.x
* pip

### Library(pip)

* requests

## How to use

### install

```
pip install requests
```

### Run

```
python3 search_interval_check.py
```
